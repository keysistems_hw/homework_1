def date(day, mounth, year):
	if mounth > 12:
		return False
	elif mounth == 2: #для февраля проверяем на високосный год
		if year%400 == 0 or (year%4 == 0 and year%100 != 0): 
			if day > 29:
				return False
		else:
			if day > 28:
				return False
	elif mounth in [1, 3, 5, 7, 8, 10, 12]: # месяцы с 31 днём
		if day > 31:
			return False
	else:
		if day > 30:
			return False
	return True



print(date(1, 1, 1))
print(date(29, 2, 400))
print(date(29, 2, 2020))
print(date(20, 7, 2022))

print(date(31, 6, 1972))
print(date(29, 2, 2300))
