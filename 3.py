def is_palindrome(text):
    text = text.replace(' ','').lower()
    return True if text == text[::-1] else False

print(is_palindrome('Привет'))
print(is_palindrome('123321'))
print(is_palindrome('А роза упала на лапу Азора'))
print(is_palindrome('Python'))