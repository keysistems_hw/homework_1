def my_func(numbers):
	unique_numbers = list(set(numbers))
	unique_numbers.reverse()
	return tuple(unique_numbers)

print(my_func([1, 2, 2, 2, 3, 4, 4, 5]))