from math import ceil, floor


def sum_range(a, z):
    result = 0
    for i in range(ceil(a), floor(z)+1):
        result += i
    return result

print(sum_range(1.2, 4.5))
print(sum_range(1, 2))